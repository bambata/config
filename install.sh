#!/usr/bin/env bash

# Repo for Vim version 8
add-apt-repository ppa:jonathonf/vim

# Update
apt-get update
apt-get upgrade -y

# Some necessary Unix tools
apt-get install -y curl unzip git stow tree vim tmux

############## Install dotfiles
git clone git@gitlab.com:bambata/config.git

############## Python world

# Install for python dev
read -p "Install python dev env ? (y/n)" yn
case $yn in
    [Yy]* )
      apt-get install -y python python-pip
      pip install virtualenvwrapper
      pip install -u awscli
esac

############## C/C++ World

# Install for C/C++ dev
read -p "Install C/C++ dev env ? (y/n)" yn
case $yn in
    [Yy]* )
      apt-get install gcc-5 g++-5 gdb valgrind
esac

# Install CUDA
read -p "Install CUDA 9.2 (Nvidia GPU needed) ? (y/n)" yn
case $yn in
    [Yy]* )
      curl -O https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_10.0.130-1_amd64.deb 9.2
      dpkg -i cuda-repo-ubuntu1604_10.0.130-1_amd64.deb
      apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
      apt-get update
      apt-get install cuda
esac

############## GoLang World

# Install go 1.11
read -p "Install go 1.11 ? (y/n)" yn
case $yn in
    [Yy]* )
      curl -O https://dl.google.com/go/go1.11.linux-amd64.tar.gz
      tar -C /usr/local -xzf go1.11.linux-amd64.tar.gz
      ## create the GOPATH
      mkdir ~/go
      export GOPATH=~/go
      export PATH=/usr/local/go/bin:$GOPATH/bin:$PATH

      # Install go dep
      read -p "Install go dep (3rd party dep management tool) ? (y/n)" yn
      case $yn in
          [Yy]* )
            source ~/.bash_profile
            curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
      esac

      # Install gocode
      read -p "Install gocode (completion for go) ? (y/n)" yn
      case $yn in
          [Yy]* )
            source ~/.bash_profile
            go get -u github.com/mdempsky/gocode
            break;;
          * ) break;;
      esac
esac

############## Javascript world

# Install nvm
read -p "Install nvm ? (y/n)" yn
case $yn in
    [Yy]* )
      curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
      nvm install 8
      nvm use 8
      break;;
    * ) break;;
esac

############## Docker world

# Install docker
read -p "Install docker ? (y/n)" yn
case $yn in
    [Yy]* )
      apt-get install apt-transport-https ca-certificates software-properties-common
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
      add-apt-repository \
         "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
         $(lsb_release -cs) \
         stable"
      apt-get install docker-ce
      systemctl start docker
      break;;
    * ) break;;
esac

############## Opencl world

read -p "install opencl ? (y/n)" yn
case $yn in
    [Yy]* )
      apt-get -y install xz-utils ocl-icd-opencl-dev ocl-icd-libopencl1 opencl-headers clinfo
      curl http://registrationcenter-download.intel.com/akdlm/irc_nas/11396/SRB5.0_linux64.zip > SRB5.0_linux64.zip
      tar xvf opencl/intel-opencl-devel-r5.0-63503.x86_64.tar.xz -C /
      tar xvf opencl/intel-opencl-cpu-r5.0-63503.x86_64.tar.xz -C /
      tar xvf opencl/intel-opencl-r5.0-63503.x86_64.tar.xz -C /
      ldconfig
      break;;
    * ) break;;
esac


############## Vim setup
mkdir -p ~/.vim/bundle
ln -s config/vim/.vimrc ~/
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
