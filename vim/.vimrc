""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vundle config
set mouse=a
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" List of plugin
Plugin 'kien/ctrlp.vim'
Plugin 'rking/ag.vim'
Plugin 'ddrscott/vim-side-search'
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'klen/python-mode'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-fugitive'
Plugin 'kchmck/vim-coffee-script'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'Chiel92/vim-autoformat'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'elmcast/elm-vim'
Plugin 'leafgarland/typescript-vim'
Plugin 'Shougo/vimproc.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'wincent/terminus'
Plugin 'Xuyuanp/nerdtree-git-plugin'

Plugin 'fatih/vim-go'
Plugin 'mdempsky/gocode', {'rtp': 'vim/'}
" Plugin 'lifepillar/pgsql'

" lint/pretty
Plugin 'w0rp/ale'

" Colorscheme
" Plugin 'tomasr/molokai'

" Language specific
Plugin 'pangloss/vim-javascript'

call vundle#end()            " required
filetype plugin indent on    " required
filetype plugin on           " required

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Global config

" Switch syntax highlighting on, when terminal has colors
syntax on
" Use vim, not vi api
set nocompatible
set mouse=a
" No Backup file
set nobackup
set nowritebackup
" No swap file
set noswapfile
" Always show cursor
set ruler
" Command history
set history=100
set background=dark
" colorscheme molokai "colorscheme colorsbox-material solarized Tomorrow-Night-Eighties lightcolors
set exrc
set laststatus=2
set cinoptions=>s,e0,n0,f0,{0,}0,^0,:s,=,l0,gs,hs,ps,ts,+s,c3,C0,(0,us,U0,w0,m0,j0,)50,*200
set bs=2 " fix the backspace
set wildmenu " visual autocomplete for command menu
set guifont=Meslo\ LG\ M\ Regular\ for\ Powerline:h12
set guioptions-=L
set guioptions-=l
set guioptions-=T
set guioptions-=r
set ttyfast " Faster redrawing.
set lazyredraw " Only redraw when necessary.
set t_Co=256 " Needed by deepsea and powerline

hi Normal ctermbg=NONE
set hidden
set nowrap
set autoread
set noswapfile
set number

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Map config
" Leader map to space
let mapleader = " "

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" grep
map <leader>g :silent grep! <C-R>=expand("<cword>")<CR><CR>
autocmd QuickFixCmdPost *grep* botright redraw! | copen
nnoremap tt :silent grep!

" Copy to clipboard
vnoremap <leader>y "*y
vnoremap <leader>Y "*Y
nnoremap <leader>y "*y
nnoremap <leader>Y "*Y
nnoremap <leader>yy "*yy

" Paste from clipboard
vnoremap <leader>p "+p
vnoremap <leader>P "+P
nnoremap <leader>p "+p
nnoremap <leader>P "+P

inoremap <Nul> <C-x><C-o>

" Run neoformat on save
" autocmd BufWritePre *.js Neoformat

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tab config set smartindent
set tabstop=2
set shiftwidth=2
set expandtab

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search config
set incsearch
set hlsearch
set ignorecase
" ignore case in search
set smartcase
nnoremap * *N


" Shortcut
map <leader>q :q!<cr>
map <C-S> :w!<cr>
map <leader>ss :source $HOME/.vimrc<cr>
map <leader>so :e ~/.vimrc<cr>
map <leader>d :Gdiff<cr>
map <leader>tf :NERDTreeFind<cr>
map <leader>tt :NERDTreeToggle<cr>

map <C-s> <esc>:w<cr>
imap <C-s> <esc>:w<cr>

" Split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Up and Down for lines
map <Alt-K> :call feedkeys( line('.')==1 ? '' : 'ddkP' )<CR>
map <Alt-J> ddp

" Resize the buffers
nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

" JS-Beautify
vmap <leader>j : ! js-beautify -s 2<CR>
map <leader>j :%! js-beautify -s 2<CR>

" Trailing space
map <leader>tr :%s/\s\+$//g<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" grep
" command! -nargs=+ MyGrep execute 'silent grep! <args>' | copen
autocmd QuickFixCmdPost *grep* botright cwindow

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree

map <leader>tf :NERDTreeFind<cr>
map <leader>tt :NERDTreeToggle<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ag
let g:ag_highlight=1
let g:ag_prg="ag --column --nogroup"
map <leader>gs :Ag<space>
map <leader>gg :Ag<space><C-R>=expand("<cword>")<CR><CR>
vmap <leader>gg y:Ag<space><C-R>"<CR><CR>
map <leader>gc :cclose<CR>
map <leader>go :botright cwindow<CR>
map <leader>gn :cNext<CR>
map <leader>gN :cprevious<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Side search
let g:side_search_prg = 'ag --word-regexp --heading --stats -C 5 --group'
map <leader>fs :SideSearch<space>
map <leader>ff :SideSearch<space><C-R>=expand("<cword>")<CR><CR>
vmap <leader>ff y:SideSearch<space><C-R>"<CR><CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree
let g:NERDTreeIndicatorMapCustom = {
      \ "Modified"  : "✹",
      \ "Staged"    : "✚",
      \ "Untracked" : "✭",
      \ "Renamed"   : "➜",
      \ "Unmerged"  : "═",
      \ "Deleted"   : "✖",
      \ "Dirty"     : "✗",
      \ "Clean"     : "✔︎",
      \ "Unknown"   : "?"
      \ }

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Explore config
" noremap <C-e> :Vexplore<CR>
let g:netrw_list_hide = '.swp$,.git$'

let g:netrw_browse_split = 0
let g:netrw_preview      = 1     " p: preview vertical split
let g:netrw_liststyle    = 3     " tree styke listing
let g:netrw_winsize      = 30    " % of the width of Explore window

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Clang-format
map <leader>f :%pyf $HOME/Repo/clang+llvm-3.7.0-x86_64-linux-gnu-ubuntu-14.04/share/clang/clang-format.py<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ctrl-p
let g:ctrlp_working_path_mode = 0   " Search file from starting directory
let g:ctrlp_max_files = 0
let g:ctrlp_max_depth = 40

if executable("ag")
   set grepprg=ag\ --vimgrep
   set grepformat=%f:%l:%c:%m
   let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
   let g:ctrlp_use_caching = 0
endif

map <leader>bs :CtrlPBuffer<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin python-mode config
let g:pymode_folding = 0
au FileType python set shiftwidth=4
au FileType python set tabstop=4

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin vim-markdown config
let g:vim_markdown_folding_disabled=1
let g:vim_markdown_new_list_item_indent = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin snipmate config
imap <C-\> <Plug>snipMateNextOrTrigger
smap <C-\> <Plug>snipMateNextOrTrigger
imap <C-\><leader> <Plug>snipMateShow

map <leader>1 :b 1<CR>
map <leader>2 :b 2<CR>
map <leader>3 :b 3<CR>
map <leader>4 :b 4<CR>
map <leader>5 :b 5<CR>
map <leader>6 :b 6<CR>
map <leader>7 :b 7<CR>
map <leader>8 :b 8<CR>
map <leader>9 :b 9<CR>

filetype off
set runtimepath+=/usr/local/Cellar/lilypond/2.18.2/share/lilypond/2.18.2/vim/
filetype on
syntax on

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ale (lint)
let g:ale_sign_column_always = 1

set clipboard=unnamedplus

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Abbreviate
ab jstr JSON.stringify(
ab clog console.log(
