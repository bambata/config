# Go specific
export GOPATH=~/go
export PATH=$GOPATH/bin:$PATH

# Python specific
export PATH=$HOME/.local/bin:$PATH
export WORKON_HOME=~/.virtualenvs
mkdir -p $WORKON_HOME
source ~/.local/bin/virtualenvwrapper.sh
